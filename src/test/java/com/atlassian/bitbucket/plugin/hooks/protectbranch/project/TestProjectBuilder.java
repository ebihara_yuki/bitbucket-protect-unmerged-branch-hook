package com.atlassian.bitbucket.plugin.hooks.protectbranch.project;

import com.atlassian.bitbucket.project.Project;
import com.atlassian.stash.internal.project.InternalNormalProject;

public class TestProjectBuilder {

    public static final Project DEFAULT_PROJECT = new TestProjectBuilder().build();

    private final InternalNormalProject.Builder builder;

    public TestProjectBuilder() {
        builder = new InternalNormalProject.Builder();
        builder.id(900).key("PROJECT").name("Project").description("Project description");
    }

    public TestProjectBuilder id(int id) {
        builder.id(id);
        return this;
    }

    public TestProjectBuilder key(String key) {
        builder.key(key);
        return this;
    }

    public TestProjectBuilder name(String name) {
        builder.name(name);
        return this;
    }

    public TestProjectBuilder description(String description) {
        builder.description(description);
        return this;
    }

    public Project build() {
        return builder.build();
    }
}
