package com.atlassian.bitbucket.plugin.hooks.protectbranch.pull;

import com.atlassian.bitbucket.pull.PullRequest;
import com.atlassian.bitbucket.pull.PullRequestRef;
import com.atlassian.bitbucket.pull.PullRequestState;
import com.atlassian.bitbucket.plugin.hooks.protectbranch.util.TestBuilderUtils;
import com.atlassian.stash.internal.pull.InternalPullRequest;
import com.atlassian.stash.internal.pull.InternalPullRequestRef;

public class TestPullRequestBuilder {

    private final InternalPullRequest.Builder builder;
    private int version;

    public TestPullRequestBuilder() {
        builder = new InternalPullRequest.Builder()
                .scopedId(398L)
                .state(PullRequestState.OPEN)
                .description("Pull Request description")
                .title("Pull Request title")
                .toRef((InternalPullRequestRef) new TestPullRequestRefBuilder().id("123").build())
                .fromRef((InternalPullRequestRef) new TestPullRequestRefBuilder().id("456").build());
        version = 1;
    }

    public TestPullRequestBuilder id(long id) {
        // External users only see the scoped ID, so we'll use that here, too.
        builder.scopedId(id);
        return this;
    }

    public TestPullRequestBuilder state(PullRequestState state) {
        builder.state(state);
        return this;
    }

    public TestPullRequestBuilder description(String description) {
        builder.description(description);
        return this;
    }

    public TestPullRequestBuilder title(String title) {
        builder.title(title);
        return this;
    }

    public TestPullRequestBuilder version(int newVersion) {
        version = newVersion;
        return this;
    }

    public TestPullRequestBuilder toRef(PullRequestRef ref) {
        builder.toRef((InternalPullRequestRef) ref);
        return this;
    }

    public TestPullRequestBuilder toRef(TestPullRequestRefBuilder refBuilder) {
        return toRef(refBuilder.build());
    }

    public TestPullRequestBuilder fromRef(PullRequestRef ref) {
        builder.fromRef((InternalPullRequestRef) ref);
        return this;
    }

    public TestPullRequestBuilder fromRef(TestPullRequestRefBuilder refBuilder) {
        return fromRef(refBuilder.build());
    }

    public PullRequest build() {
        InternalPullRequest pr = builder.build();
        TestBuilderUtils.setPrivateField(pr, "version", version);
        return pr;
    }
}
